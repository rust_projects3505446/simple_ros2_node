# Using Parameters with [ros2_rust](https://github.com/ros2-rust/ros2_rust)
This hands-on tutorial will guide you through the utilization of [ROS 2](https://docs.ros.org/en/humble/index.html) 
[parameters](https://docs.ros.org/en/humble/Tutorials/Beginner-CLI-Tools/Understanding-ROS2-Parameters/Understanding-ROS2-Parameters.html) within the 
[Rust programming language](https://doc.rust-lang.org/book/) for robotic applications. The focus will 
be on practical implementation strategies and potential considerations that may arise during development.  

## About Ros2 parameters
ROS 2 parameters are used to configure [nodes](https://docs.ros.org/en/humble/Tutorials/Beginner-CLI-Tools/Understanding-ROS2-Nodes/Understanding-ROS2-Nodes.html) at runtime, 
without needing to change the code itself. This allows for:
* **Flexibility**: You can start a node with different settings depending on the situation, by providing different parameter values.
* **Maintainability**: Code stays clean and unchanged, as configuration is handled externally.
* **Simplified Testing**: Different parameter sets can be used to test the node under various conditions.

Overall, parameters improve the flexibility and maintainability of ROS 2 nodes.
<details><summary>The Startpoint</summary>

Let's have a look at the code of the following publisher:
```rust
use rclrs::{create_node, Context, Node, Publisher, RclrsError, QOS_PROFILE_DEFAULT};
/// Creates a SimplePublisherNode, initializes a node and publisher, and provides
/// methods to publish a simple "Hello World" message on a loop in separate threads.

/// Imports the Arc type from std::sync, used for thread-safe reference counting pointers,
/// and the StringMsg message type from std_msgs for publishing string messages.
use std::{iter, sync::Arc, thread, time::Duration};
use std_msgs::msg::String as StringMsg;
// / SimplePublisherNode struct contains node and publisher members.
// / Used to initialize a ROS 2 node and publisher, and publish messages.
struct SimplePublisherNode {
    node: Arc<Node>,
    _publisher: Arc<Publisher<StringMsg>>,
}
/// Creates a new SimplePublisherNode by initializing a node and publisher.
///
/// The `new` function takes a context and returns a Result containing the
/// initialized SimplePublisherNode or an error. It creates a node with the
/// given name and creates a publisher on the "publish_hello" topic.
///
/// The SimplePublisherNode contains the node and publisher members.
impl SimplePublisherNode {
    /// Creates a new SimplePublisherNode by initializing a node and publisher.
    ///
    /// This function takes a context and returns a Result containing the
    /// initialized SimplePublisherNode or an error. It creates a node with the
    /// given name and creates a publisher on the "publish_hello" topic.
    ///
    /// The SimplePublisherNode contains the node and publisher members.
    fn new(context: &Context) -> Result<Self, RclrsError> {
        let node = create_node(context, "simple_publisher").unwrap();
        let _publisher = node
            .create_publisher("publish_hello", QOS_PROFILE_DEFAULT)
            .unwrap();
        Ok(Self { node, _publisher })
    }

    /// Publishes a "Hello World" message on the publisher.
    ///
    /// Creates a StringMsg with "Hello World" as the data, publishes it on
    /// the `_publisher`, and returns a Result. This allows regularly publishing
    /// a simple message on a loop.
    fn publish_data(&self, increment: i32) -> Result<i32, RclrsError> {
        let msg: StringMsg = StringMsg {
            data: format!("Hello World {}", increment),
        };
        self._publisher.publish(msg).unwrap();
        Ok(increment + 1_i32)
    }
}

/// The main function initializes a ROS 2 context, node and publisher,
/// spawns a thread to publish messages repeatedly, and spins the node
/// to receive callbacks.
///
/// It creates a context, initializes a SimplePublisherNode which creates
/// a node and publisher, clones the publisher to pass to the thread,  
/// spawns a thread to publish "Hello World" messages repeatedly, and
/// calls spin() on the node to receive callbacks. This allows publishing
/// messages asynchronously while spinning the node.
fn main() -> Result<(), RclrsError> {
    let context = Context::new(std::env::args()).unwrap();
    let publisher = Arc::new(SimplePublisherNode::new(&context).unwrap());
    let publisher_other_thread = Arc::clone(&publisher);
    let mut count: i32 = 0;
    thread::spawn(move || -> () {
        iter::repeat(()).for_each(|()| {
            thread::sleep(Duration::from_millis(1000));
            count = publisher_other_thread.publish_data(count).unwrap();
        });
    });
    rclrs::spin(publisher.node.clone())
}
```
This is a valid publisher node written with [`rclrs`](https://docs.rs/rclrs/latest/rclrs/) using a 
[`std_msgs::msg::String`](http://docs.ros.org/en/melodic/api/std_msgs/html/msg/String.html) as the publisher 
[topic](https://docs.ros.org/en/foxy/Tutorials/Beginner-CLI-Tools/Understanding-ROS2-Topics/Understanding-ROS2-Topics.html) type. Let's take another look at the `fn new` method:
```rust
fn new(context: &Context) -> Result<Self, RclrsError> {
    let node = create_node(context, "simple_publisher").unwrap();
    let _publisher = node
        .create_publisher("publish_hello", QOS_PROFILE_DEFAULT)
        .unwrap();
    Ok(Self { node, _publisher })
}

```
In this method, the default name of the node is defined, in this case "simple_publisher" and in the method `create_publisher` 
the name of the topic which is served by the publisher. In this particular case, the name of the topic is static, i.e. it will 
be the same for every node instance created from this programme. 
## Integration of parameters in the code:
Parameters can now be used to change this name. It may then 
look like this:
```rust
fn new(context: &Context) -> Result<Self,RclrsError> {
    let node = create_node(context, "simple_publisher").unwrap();
    let _publisher = node
        .create_publisher(
            &node.declare_parameter("topic_name")
            .default(Arc::from("publish_hello"))
            .description("The name of the topic to be published on. Has a default value.")
            .mandatory()
            .unwrap().get(), QOS_PROFILE_DEFAULT)
        .unwrap();
    Ok(Self { node, _publisher, })
}
```
This code snippet defines a function named new that initializes a ROS 2 node and a publisher. Here's a breakdown of the code with a focus on the declare_parameter method:
### Parameter Declaration:
`node.declare_parameter`: [This method](https://docs.rs/rclrs/latest/rclrs/struct.Node.html#method.declare_parameter) from the 
`rclrs` library is used to declare a ROS 2 parameter associated with the node. 
It takes the parameter name (`"topic_name"`) as an argument.
### Parameter Configuration (Chained Calls):
* `.default(Arc::from("publish_hello"))`: Sets the 
[default value](https://docs.rs/rclrs/latest/rclrs/struct.ParameterBuilder.html#method.default) for the parameter as 
`"publish_hello"`. [Arc](https://doc.rust-lang.org/std/sync/struct.Arc.html) is a Rust reference counting type used here.
* `.description("... " )`: Provides a human-readable 
[description](https://docs.rs/rclrs/latest/rclrs/struct.ParameterBuilder.html#method.description) for the parameter.
* `.mandatory()`: Makes the parameter 
[mandatory](https://docs.rs/rclrs/latest/rclrs/struct.ParameterBuilder.html#method.mandatory). This means the node won't be 
created unless a value for this parameter is provided. In this particular case, there is already a default value.
* `.unwrap()`: Unwraps the result of the chained calls, assuming no errors occurred during configuration. This can be risky 
in production code, consider using error handling instead.
* `get()`: [Retrieves the actual value](https://docs.rs/rclrs/latest/rclrs/struct.Parameters.html#method.get) of the declared 
parameter (`"topic_name"`).
Overall, the declare_parameter method simplifies configuration management and improves the 
flexibility of ROS 2 nodes written in Rust using the rclrs library.
</details>

<details><summary>A Publisher with parameters:</summary>

Having explained how to make a publisher node parameterisable in ROS 2, 
[here](https://gitlab.com/ros21923912/simple_ros2_node/-/blob/more_simple_nodes/src/param_publisher.rs?ref_type=heads) is the code for the publisher:
```rust
use rclrs::{create_node, Context, Node, Publisher, RclrsError, QOS_PROFILE_DEFAULT};
/// A simple ROS2 publisher node that publishes a "Hello World" message at a 1 Hz rate.
///
/// This node creates a publisher that publishes a "Hello World" message to a topic specified by the `topic_name` parameter. The message is published at a rate of 1 Hz in a 
/// separate thread.
///
/// # Example
use std::{sync::Arc, thread, time::Duration};
use std_msgs::msg::String as StringMsg;

/// A simple ROS2 publisher node that publishes a "Hello World" message at a 1 Hz rate.
///
/// This struct represents a ROS2 publisher node that publishes a "Hello World" message to a topic specified by the `topic_name` parameter. The message is published at a rate of 
/// 1 Hz in a separate thread.
struct SimplePublisherNode {
    node: Arc<Node>,
    _publisher: Arc<Publisher<StringMsg>>,
}

/// A simple ROS2 publisher node that publishes a "Hello World" message at a 1 Hz rate.
///
/// The `new` function creates the publisher node and returns a `SimplePublisherNode` instance. The 
/// `publish_data` function publishes the "Hello World" message with an incrementing counter.
impl SimplePublisherNode {
    /// Creates a new `SimplePublisherNode` instance.
    ///
    /// This function creates a new ROS 2 node named "simple_publisher" and a publisher for the topic specified by the "topic_name" parameter. 
    /// The publisher publishes a "Hello World" message at a rate of 1 Hz.
    ///
    /// # Arguments
    /// * `context` - The ROS2 context to use for creating the node.
    ///
    /// # Returns
    /// A new `SimplePublisherNode` instance, or an error if the node or publisher could not be created.
    fn new(context: &Context) -> Result<Self, RclrsError> {
        let node = create_node(context, "simple_publisher").unwrap();
        let _publisher = node
            .create_publisher(
                &node
                    .declare_parameter("topic_name")
                    .default(Arc::from("publish_hello"))
                    .description("The name of the topic to be published on. Has a default value.")
                    .mandatory()
                    .unwrap()
                    .get(),
                QOS_PROFILE_DEFAULT,
            )
            .unwrap();
        Ok(Self { node, _publisher })
    }

    /// Publishes a "Hello World" message with an incrementing counter.
    ///
    /// This function publishes a "Hello World" message with an incrementing counter to the topic specified by the "topic_name" parameter. The message is published using the 
    /// publisher created in the `new` function.
    ///
    /// # Arguments
    /// * `increment` - The current value of the incrementing counter.
    ///
    /// # Returns
    ///
    /// The updated value of the incrementing counter.
    fn publish_data(&self, increment: i32) -> Result<i32, RclrsError> {
        let msg: StringMsg = StringMsg {
            data: format!("Hello World {}", increment),
        };
        self._publisher.publish(msg).unwrap();
        Ok(increment + 1_i32)
    }
}

/// The `main` function creates a new ROS2 context, a `SimplePublisherNode` instance, and starts a new thread to publish "Hello World" messages at a 1 Hz rate.
///
/// The `SimplePublisherNode` instance is created using the `new` function, which sets up a ROS2 node named "simple_publisher" and a publisher for the topic specified by the 
/// "topic_name" parameter.
///
/// The new thread is spawned using `thread::spawn`, which calls the `publish_data` function of the `SimplePublisherNode` instance every 1 second. The `publish_data` function 
/// publishes a "Hello World" message with an incrementing counter.
///
/// Finally, the `rclrs::spin` function is called to keep the ROS 2 node running and processing messages.
fn main() -> Result<(), RclrsError> {
    let context = Context::new(std::env::args()).unwrap();
    let publisher = Arc::new(SimplePublisherNode::new(&context).unwrap());
    let publisher_other_thread = Arc::clone(&publisher);
    let mut count: i32 = 0;
    thread::spawn(move || loop {
        thread::sleep(Duration::from_millis(1000));
        count = publisher_other_thread.publish_data(count).unwrap();
    });
    rclrs::spin(publisher.node.clone())
}
```
This publisher now uses a parameter for the name of its topic.

</details>
<details><summary> The Paremeterizable Subscriber</summary>

Because a publisher alone with a parameterisable topic name is boring and more difficult to test, 
a subscriber has now been [added](https://gitlab.com/ros21923912/simple_ros2_node/-/blob/more_simple_nodes/src/param_subscriber.rs?ref_type=heads):
```rust
use rclrs::{create_node, Context, Node, RclrsError, Subscription, QOS_PROFILE_DEFAULT};
use std::{
    env,
    sync::{Arc, Mutex},
    time::Duration,
    thread,
};
use std_msgs::msg::String as StringMsg;
/// A simple ROS 2 subscriber node that receives and prints "hello" messages.
///
/// This node creates a subscription to the "publish_hello" topic and prints the
/// received messages to the console. It runs the subscription in a separate
/// thread, while the main thread calls `rclrs::spin()` to keep the node running.
pub struct SimpleSubscriptionNode {
    node: Arc<Node>,
    _subscriber: Arc<Subscription<StringMsg>>,
    data: Arc<Mutex<Option<StringMsg>>>,
}
/// Implements a simple ROS 2 subscriber node that receives and prints "hello" messages.
///
/// The `SimpleSubscriptionNode` creates a subscription to the "publish_hello" topic and
/// prints the received messages to the console. It runs the subscription in a separate
/// thread, while the main thread calls `rclrs::spin()` to keep the node running.
///
/// The `new` function creates the node and the subscription, and returns a `SimpleSubscriptionNode`
/// instance. The `data_callback` function can be used to access the latest received message.
impl SimpleSubscriptionNode {
    fn new(context: &Context) -> Result<Self, RclrsError> {
        let node = create_node(context, "simple_subscription").unwrap();
        let data: Arc<Mutex<Option<StringMsg>>> = Arc::new(Mutex::new(None));
        let data_mut: Arc<Mutex<Option<StringMsg>>> = Arc::clone(&data);
        let _subscriber = node
            .create_subscription::<StringMsg, _>(
                &node.declare_parameter("topic_name")
                .default(Arc::from("publish_hello"))
                .description("The name of the topic to be subscribed on. Has a default value.")
                .mandatory()
                .unwrap().get(),
                QOS_PROFILE_DEFAULT,
                move |msg: StringMsg| {
                    *data_mut.lock().unwrap() = Some(msg);
                },
            )
            .unwrap();
        Ok(Self {
            node,
            _subscriber,
            data,
        })
    }
    fn data_callback(&self) -> Result<(), RclrsError> {
        if let Some(data) = self.data.lock().unwrap().as_ref() {
            println!("{}", data.data);
        } else {
            println!("No message available yet.");
        }
        Ok(())
    }
}
/// The `main` function creates a new ROS 2 context, a `SimpleSubscriptionNode` instance, and starts a separate thread to periodically call the `data_callback` method on the 
/// subscription. The main thread then calls `rclrs::spin()` to keep the node running and receive messages.
///
/// The separate thread is used to ensure that the `data_callback` method is called regularly, even if the main thread is blocked in `rclrs::spin()`. This allows the subscriber 
/// to continuously process and print the received "hello" messages.
fn main() -> Result<(), RclrsError> {
    let context = Context::new(env::args()).unwrap();
    let subscription = Arc::new(SimpleSubscriptionNode::new(&context).unwrap());
    let subscription_other_thread = Arc::clone(&subscription);
    thread::spawn(move || loop{ 
            thread::sleep(Duration::from_millis(1000));
            subscription_other_thread.data_callback().unwrap()
    });
    rclrs::spin(subscription.node.clone())
}
```
As you can see, the topic name can also be parameterised for the subscriber of this node.

</details>
<details><summary>A test with launch files</summary>

Roughly speaking, [launch files](https://design.ros2.org/articles/roslaunch.html) in ROS 2 are
used for composing several nodes in order to assign special names or parameters to them, e.g. 
for system startup. Here they are used to assign the same topic name in the subscriber and 
publisher to the two nodes at system startup. The 
[following launch](https://gitlab.com/ros21923912/simple_ros2_node/-/blob/more_simple_nodes/launch/param_launch.py?ref_type=heads) file will do the trick:
```py
from launch import LaunchDescription
from launch_ros.actions import Node
param_val=[
    {'topic_name': 'param_hello'}
]
def generate_launch_description():
    return LaunchDescription([
        Node(
            package='simple_ros_rs',
            executable='param_publisher',
            name='param_pub',
            output='screen',
            emulate_tty=True,
            parameters=param_val,
        ),
        Node(
            package='simple_ros_rs',
            executable='param_subscriber',
            name='param_sub',
            output='screen',
            emulate_tty=True,
            parameters=param_val,
        )
    ])
```
<details><summary>Examining the Code</summary>

1. Parameter Definition:This dictionary or associative list is used to assign the correct value to the parameters of the nodes. In this case, only the value for the topic name is assigned.
2. `generate_launch_description`: This function defines the launch configuration. It returns a 
`LaunchDescription` object.
3. Node Definitions (using list comprehension):
    * The function uses list comprehension to create two `Node` objects and add them to a list.
    * Each `Node` object defines the configuration for a ROS 2 node to be launched.
4. Node Configuration:
    * `package='simple_ros_rs'`: Specifies the package name containing the node executables (`param_publisher` and `param_subscriber`).
    * executable: The name of the executable within the package to be launched (either `param_publisher` or `param_subscriber`).
    * `name`: The name assigned to the launched node (either `param_pub` or `param_sub`).
    * `output='screen'`: Prints the node's output to the terminal window.
    * `emulate_tty=True`: Simulates a terminal for the node (helpful for interactive nodes).
    * `parameters=param_val`: This passes the defined parameter list (`param_val`) to both nodes.
5. Returning Launch Description: The function returns the `LaunchDescription` object containing the configuration for launching the two nodes with the specified parameters.

Overall, this launch file defines two nodes, a publisher (`param_publisher`) and a subscriber (`param_subscriber`), from the 
`simple_ros_rs` package. Both nodes will share the same parameter, `"topic_name"` set to `"param_hello"`. Launching this file 
will start both nodes simultaneously, allowing them to communicate with each other using the specified topic.

</details>

In order to start these two rclrs nodes with this launch file, the package must of course first be properly compiled:
```sh
cd WORKSPACEFOLDER
colcon build --packages-select your_package_name
source install/setup.bash
```
The launch file can now be used. Normally it looks like this:
```sh
ros2 launch your_package_name your_launch_file.py
```
It can happen that a launch file is not correctly linked to the corresponding package. This is not a problem, as the launch 
file can still be accessed:
```sh
ros2 launch /path_to_your_launch_file/your_launch_file.py
```
If everything went well when compiling and starting the launch file, the result should look like this:
```sh
[param_subscriber-2] Hello World 1
[param_subscriber-2] Hello World 2
[param_subscriber-2] Hello World 3
[param_subscriber-2] Hello World 4
[param_subscriber-2] Hello World 5
[param_subscriber-2] Hello World 6
[param_subscriber-2] Hello World 7
[param_subscriber-2] Hello World 8
[param_subscriber-2] Hello World 9
[param_subscriber-2] Hello World 10
```

</details>
<details><summary>Last thoughts</summary>

**ROS 2 Parameter Configuration with rclrs**: The rclrs library incorporates ROS 2 parameters, offering a refined and developer-centric approach for configuring
 nodes during runtime. This approach aligns with the design principles observed in popular parameter management libraries like 
[generate_parameter_library](https://github.com/PickNikRobotics/generate_parameter_library) for Python and C/C++.

**RO S 2 Launch Files for Streamlined Node Management**: Launch files present a sophisticated solution for configuring and 
initiating multiple ROS 2 nodes within a cohesive environment. Their use facilitates streamlined development workflows and 
enhances overall application management.

</details>
