use rclrs::{create_node, Context, Node, Publisher, RclrsError, QOS_PROFILE_DEFAULT};
/// Creates a SimplePublisherNode, initializes a node and publisher, and provides
/// methods to publish a simple "Hello World" message on a loop in separate threads.

/// Imports the Arc type from std::sync, used for thread-safe reference counting pointers,
/// and the StringMsg message type from std_msgs for publishing string messages.
use std::{env, sync::Arc, thread, time::Duration};
use std_msgs::msg::String as StringMsg;

/// A simple ROS2 publisher node that publishes a "Hello World" message at a 1 Hz rate.
///
/// This struct represents a ROS2 publisher node that publishes a "Hello World" message to a topic specified by the `topic_name` parameter. The message is published at a rate of 1 Hz in a separate thread.
struct SimplePublisherNode {
    node: Arc<Node>,
    _publisher: Arc<Publisher<StringMsg>>,
}

/// A simple ROS2 publisher node that publishes a "Hello World" message at a 1 Hz rate.
///
/// The `new` function creates the publisher node and returns a `SimplePublisherNode` instance. The
/// `publish_data` function publishes the "Hello World" message with an incrementing counter.
impl SimplePublisherNode {
    /// Creates a new `SimplePublisherNode` instance.
    ///
    /// This function creates a new ROS2 node named "simple_publisher" and a publisher for the topic specified by the "topic_name" parameter.
    /// The publisher publishes a "Hello World" message at a rate of 1 Hz.
    ///
    /// The SimplePublisherNode contains the node and publisher members.
    fn new(context: &Context) -> Result<Self, RclrsError> {
        let node = create_node(context, "simple_publisher").unwrap();
        let _publisher = node
            .create_publisher(
                &node
                    .declare_parameter("topic_name")
                    .default(Arc::from("publish_hello"))
                    .description("The name of the topic to be published on. Has a default value.")
                    .mandatory()
                    .unwrap()
                    .get(),
                QOS_PROFILE_DEFAULT,
            )
            .unwrap();
        Ok(Self { node, _publisher })
    }

    /// Publishes a "Hello World" message with an incrementing counter.
    ///
    /// Creates a StringMsg with "Hello World" as the data, publishes it on
    /// the `_publisher`, and returns a Result. This allows regularly publishing
    /// a simple message on a loop.
    fn publish_data(&self, increment: i32) -> Result<i32, RclrsError> {
        self._publisher
            .publish(StringMsg {
                data: format!("Hello World {}", increment),
            })
            .unwrap();
        Ok(increment + 1_i32)
    }
}

/// The main function initializes a ROS 2 context, node and publisher,
/// spawns a thread to publish messages repeatedly, and spins the node
/// to receive callbacks.
///
/// It creates a context, initializes a SimplePublisherNode which creates
/// a node and publisher, clones the publisher to pass to the thread,  
/// spawns a thread to publish "Hello World" messages repeatedly, and
/// calls spin() on the node to receive callbacks. This allows publishing
/// messages asynchronously while spinning the node.

fn main() -> Result<(), RclrsError> {
    let publisher =
        Arc::new(SimplePublisherNode::new(&Context::new(env::args()).unwrap()).unwrap());
    let publisher_other_thread = Arc::clone(&publisher);
    let mut count: i32 = 0;
    thread::spawn(move || loop {
        thread::sleep(Duration::from_millis(1000));
        count = publisher_other_thread.publish_data(count).unwrap();
    });
    rclrs::spin(publisher.node.clone())
}
