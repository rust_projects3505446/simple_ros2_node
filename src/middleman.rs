use rclrs::{Context, Node, Publisher, RclrsError, Subscription, QOS_PROFILE_DEFAULT};
use std::{
    env,
    sync::{Arc, Mutex},
    thread,
    time::Duration,
};

use std_msgs::msg::String as StringMsg;
struct RepublisherNode {
    node: Arc<Node>,
    publisher: Arc<Publisher<StringMsg>>,
    _subscription: Arc<Subscription<StringMsg>>,
    data: Arc<Mutex<Option<StringMsg>>>,
}

impl RepublisherNode {
    fn new(context: &Context) -> Result<Self, RclrsError> {
        let node: Arc<Node> = Node::new(context, "republisher")?;
        let data = Arc::new(Mutex::new(None));
        let data_cb = Arc::clone(&data);
        let publisher = node.create_publisher("out_topic", QOS_PROFILE_DEFAULT)?;
        let _subscription =
            node.create_subscription("in_topic", QOS_PROFILE_DEFAULT, move |msg: StringMsg| {
                *data_cb.lock().unwrap() = Some(msg);
            })?;
        Ok(Self {
            node,
            publisher,
            _subscription,
            data,
        })
    }
    fn republish(&self) -> Result<(), RclrsError> {
        if let Some(s) = &*self.data.lock().unwrap() {
            self.publisher.publish(s)?;
        }
        Ok(())
    }
}
fn main() -> Result<(), RclrsError> {
    let republisher = Arc::new(RepublisherNode::new(&Context::new(env::args()).unwrap())?);
    let republisher_other_thread = Arc::clone(&republisher);
    thread::spawn(move || -> Result<(), RclrsError> {
        loop {
            thread::sleep(Duration::from_millis(1000));
            republisher_other_thread.republish()?;
        }
    });
    rclrs::spin(Arc::clone(&republisher.node))
}
