<img src="./images/Logo-ROS_2-Main.jpg" alt="../images/Logo-ROS_2-Main.jpg" height="100"/> <img src="./images/ferris.jpg" alt="../images/ferris.jpg" height="100"/> 
# simple_ros_rs
The aim of this project is to familiarise people with the [ROS2](https://docs.ros.org) 
[RUST](https://www.rust-lang.org/) api - 
[rclrs](https://github.com/ros2-rust/ros2_rust) - and at the same time to provide more and more application examples for it. All binary targets contained in this project can currently only be compiled with a toolchain customised for ROS2 and are therefore firmly connected to the ROS2 ecosystem.
## Getting started
Before you can do anything with this project, you first have to install a few dependencies:
### installation process:
Installing ros2 can sometimes be a pain. So be patient. There are several ways to do this:
<details><summary> <b>The Native installation</b></summary>
 I only recommend this if you're using 
 [Ubuntu](https://ubuntu.com/desktop) [Linux](https://www.linux.org/) as your main operating system. Please note that even if you're using [Ubuntu 23.04](https://ubuntu.com/download/desktop/thank-you?version=23.10.1&architecture=amd64) or higher, you should stick to the stable release of ros2, which is currently the 
 [humble hawksbill](https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debians.html). After installing ROS2, you'll also need to install ROS2 RUST, using the 
 [installation process](https://github.com/ros2-rust/ros2_rust/blob/main/README.md) provided by 
 its main repository.
</details>
<details><summary> <b>A containerised environment.</b></summary>
I highly recommend this as I use ROS2 in this 
 [way myself](https://gitlab.com/docker5348/ros2_main_dev_container). Despite the ros2 
 [Docker](https://www.docker.com/) container I provide, I'll recommend several others:  

* [ROS2 RUST](https://github.com/ros2-rust/ros2_rust/blob/main/Dockerfile) Obviously. It's the easiest way to get ros2 rust up and running.
* [Control](https://github.com/ros-controls/ros2_control/tree/humble/.docker) Maybe later you'll want to try out the ROS control, and then you can incorporate parts of this Docker file into your own.
* [ROS Open Container Initiative Images](https://github.com/sloretz/ros_oci_images) I really like these Docker containers and 
    they are more actual than the current OSRF containers.
* [OSRF Containers](https://github.com/osrf/docker_images) They provide a smooth baseline from which to work.
</details>

### Compiling
Once you've successfully installed ROS2 and ROS2 RUST, you need to load this project into your project:
```
git clone -b more_simple_nodes https://gitlab.com/ros21923912/simple_ros2_node ${Path to your Workspace}src/simple_ros2_node
```
Then you'll need to compile your workspace with and source the ROS2 installation with:
```
colcon build
source install/setup.bash
```
Then you'll be able to run some nodes!
<details><summary>Working with several workspaces</summary>
If you have parts of your ROS2 dependency installation in different workspaces - as I do - you'll need to use an extended 
colcon command. For example


```
colcon build \ 
        --build-base /ros_ws/build \ 
        --install-base /ros_ws/install \
        --base-paths /ros_default /ros_ws /ros_debug_ws /microros_ws

```
That's from the docker container I use. As you can see, this `colcon build` command will compile the package from multiple workspaces at once, as declared behind the `--base-paths` flag, and set the build target to the path declared behind the `--build-base`. The corresponding `setup.bash` can be found in the path specified after the `--install-base` flag.
You'll be able to source your installation in this example with:
```
source /ros_ws/install/setup.bash
```

</details>

### Running the Nodes
Once you've successfully compiled the packages and sourced your installation, you have several options for running the nodes:

#### The Classic way:
Once the compilation process is complete, you'll have fully functional ros2 nodes. They will be recognised by the ROS2 command line tool. You'll be able to run them with:
```
ros2 run simple_ros_rs ${The Node you want to run}
```
#### Using the included lauch files
The package also includes a launch file that will start the `simple_publisher` and `simple_subscriber` at once. The command 
for this is:
```
ros2 launch ${path to the package}\launch\pub_sub_launch.py
```
Launch files are designed to build compose ros2 nodes at once. It doesn't matter where a launch file is located. You'll 
be able to run any node from any package on any launch file. They're very similar to a Docker compose file.
