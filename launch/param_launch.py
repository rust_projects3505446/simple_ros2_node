from launch import LaunchDescription
from launch_ros.actions import Node
from enum import Enum


launch_package='simple_ros_rs'
screen_output='screen'
emulation=True
param_val=[
    {'topic_name': 'param_hello'}
]
def generate_launch_description():
    return LaunchDescription([
        Node(
            name='param_pub',
            executable='param_publisher',
            package='simple_ros_rs',
            output='screen',
            emulate_tty=emulation,
            parameters=param_val,
        ),
        Node(
            name='param_sub',
            executable='param_subscriber',
            package=launch_package,
            output=screen_output,
            emulate_tty=emulation,
            parameters=param_val,
        )
    ])
